<?php

/**
 * Provides AzKiVam API.
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 * @since 5 Jan 2023
 */
class AzKiVam {

  /**
   * Azki base URL.
   */
  public const BASE_URL = 'https://api.azkiloan.com';

  /**
   * @var string AzKi API Key
   */
  private string $apiKey;

  /**
   * @var string AzKi Merchant ID
   */
  private string $merchantId;

  /**
   * @var callable Custom Error handler function. Will be called on curl errors.
   */
  private $customErrorHandler;

  /**
   * constructor
   */
  public function __construct(string $apiKey, string $merchantId, $error_handler = NULL) {
    $this->apiKey = $apiKey;
    $this->merchantId = $merchantId;
    $this->customErrorHandler = $error_handler;
  }

  /**
   * Perform a CreateTicket request.
   *
   * @param int $amount Total amount per Rials.
   * @param string $redirect Return URL (when returning back from the gateway).
   * @param string $provider_id A random numeric string.
   * @param string $merchant_id The merchant ID.
   * @param string $mobile_number Customer's mobile number.
   * @param array $items Array if ordered items.
   *   example array:
   *   [
   *     0 => [
   *       'name'   => 'Name of the ordered item',
   *       'count'  => 'Count of the ordered item.',
   *       'amount' => 'Price for this item.',
   *       'url'    => 'URL of this item',
   *     ]
   *   ]
   *
   * @return ?array
   */
  public function createTicket(int $amount, string $redirect, string $provider_id, string $merchant_id, string $mobile_number, array $items): ?array {
    return $this->doRequest( '/payment/purchase', [
      'amount'        => $amount,
      'redirect_uri'  => $redirect,
      'fallback_uri'  => $redirect,
      'provider_id'   => $provider_id,
      'merchant_id'   => $merchant_id,
      'mobile_number' => $mobile_number,
      'items'         => $items,
    ]);
  }

  /**
   * verify a ticket (after returning from gateway.
   */
  public function verifyTicket($ticketId) {
    return $this->doRequest('/payment/verify', ['ticket_id' => $ticketId]);
  }

  /**
   * Perform a POST request.
   *
   * @param string $action The sub URL. e.g. "/payment/purchase"
   * @param array $data The data array.
   *
   * @return ?array
   */
  public function doRequest(string $action, array $data): ?array {
    try {
      $signature    = $this->buildSignature($action, 'POST');
      $data_string  = json_encode($data);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $this::BASE_URL . $action);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/json',
        'Signature: ' . $signature,
        'MerchantId: ' . $this->merchantId,
        'Content-Length: ' . strlen($data_string),
      ]);

      $result = curl_exec($ch);
      $error = curl_error($ch);
      curl_close($ch);

      if ($error) {
        $this->customErrorHandler($error);
        return NULL;
      }

      return json_decode($result, TRUE);
    }
    catch (\Exception $e) {
      $this->customErrorHandler($e->getMessage());

      return NULL;
    }
  }

  /**
   * Build signature for API calls.
   *
   * @param string $sub_url The sub part of the URL (e.g. "/payment/purchase")
   * @param string $request_method The request method. either "GET" or "POST"
   *
   * @return string
   */
  public function buildSignature(string $sub_url, string $request_method): string {
    $plain  = $sub_url . '#' . time() . '#' . $request_method . '#' . $this->apiKey;
    $key    = hex2bin($this->apiKey);

    // TODO: generate an IV for security (do we need this?)
    $iv = '';
    $digest = @openssl_encrypt($plain, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);

    return bin2hex($digest);
  }

  /**
   * Translate error codes. The returned strings are English.
   *
   * @param int $error The error code.
   *
   * @return string
   */
  public function translateErrorCodes(int $error): string {
    switch ($error) {
      case 0:  return "Request finished successfully.";
      case 1:  return "An internal error occurred.";
      case 2:  return "Resource Not Found.";
      case 4:  return "Malformed Data.";
      case 5:  return "Data Not Found.";
      case 12: return "The shop is not active.";
      case 13: return "Invalid mobile number.";
      case 15: return "Access Denied.";
      case 16: return "Transaction already reversed.";
      case 17: return "Ticket Expired.";
      case 18: return "Signature Invalid.";
      case 19: return "Ticket is not payable.";
      case 20: return "Client's mobile number is not the same as registered in the AzKiVam gateway.";
      case 21: return "Insufficient Credit.";
      case 28: return "Transaction cannot be verified due to status.";
      case 32: return "Invalid Invoice Data.";
      case 33: return "Contract is not started.";
      case 34: return "Contract is expired.";
      case 44: return "Validation exception.";
      case 51: return "Request data is not valid.";
      case 59: return "Transaction not reversible.";
      case 60: return "Transaction must be in verified state.";
      default: return "Unknown error.";
    }
  }

  /**
   * Default error handler for curl errors.
   *
   * Override this method, by providing the "error_handler" parameter to the constructor.
   */
  protected function customErrorHandler(string $error) {
    if (is_callable($this->customErrorHandler)) {
      ($this->customErrorHandler)($error);
    }
  }
}
